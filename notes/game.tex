In this section we fix~$k \in \mathbb{N}$.

\paragraph*{Configurations and Möbius functions.}
    A \emph{configuration} is a subset of~$2^{[k]}$.  Given a
configuration~$\mathbf{s} \subseteq 2^{[k]}$, its associated \emph{Möbius
function}~$\mu_\mathbf{s} : 2^{[k]} \to \ZZ$ is defined by bottom-up
induction as follows:
\[\mu_\mathbf{s}(n)\defeq \begin{cases}
                           1 - \sum_{n' \subsetneq n} \mu_\mathbf{s}(n')\text{ if }n \in \mathbf{s}\\
                           - \sum_{n' \subsetneq n} \mu_\mathbf{s}(n')\text{ if }n \notin \mathbf{s}
                          \end{cases}. \]
In particular, the value $\mu_\mathbf{s}(\emptyset)$ is~$1$ if $\emptyset \in
\mathbf{s}$, and $0$ if $\emptyset \notin \mathbf{s}$.

\begin{example}
\label{expl:0111001001110010}
    Let~$k=4$, and~$\mathbf{s}$ be the configuration~$\{\{0\}, \{1\}, \{0,1\},
\{0,3\}, \{1,2\}, \{1,3\}, \allowbreak \{0,1,3\}, \{1,2,3\}  \}$. We have depicted in
Figure~\ref{fig:0111001001110010} this configuration and its associated Möbius
function.
\end{example}

\begin{remark}
    Let~$\mu: 2^{[k]} \to \ZZ$. Then~$\mu$ is the Möbius function of
some configuration if and only if the following holds: for all~$n\in 2^{[k]}$,
we have~$\sum_{n' \subseteq n} \mu(n') \in \{0,1\}$. The associated
configuration is then simply $\{n \in 2^{[k]} \mid \sum_{n' \subseteq n}
\mu(n') = 1\}$.
\end{remark}

\begin{figure}
\begin{center}
\input{figures/0111001001110010.tex}
\label{fig:0111001001110010}
\caption{Visual representation of the configuration~$\mathbf{s}$ from
Example~\ref{expl:0111001001110010} and of its associated Möbius
function~$\mu_\mathbf{s}$. For simplicity, we write, e.g., “$01: -1$” to mean
that~$\mu_\mathbf{s}(\{0,1\}) = -1$. Here, $\mathbf{s}$ consists of all the
colored nodes.}
\end{center}
\end{figure}

\paragraph*{Disjoint unions and differences.}
    Let~$\mathbf{s_1},\mathbf{s_2}$ be two configurations. When~$\mathbf{s_1} \cap
\mathbf{s_2} = \emptyset$, we define the \emph{disjoint union of~$\mathbf{s_1}$
and~$\mathbf{s_2}$} by~$\mathbf{s_1} \oplus \mathbf{s_2} \defeq \mathbf{s_1}
\cup \mathbf{s_2}$. When~$\mathbf{s_2} \subseteq \mathbf{s_1}$, we define the
\emph{subset complement of~$\mathbf{s_1}$ and~$\mathbf{s_2}$}
by~$\mathbf{s_1} \ominus \mathbf{s_2} \defeq \mathbf{s_1} \setminus
\mathbf{s_2}$.  From now on, when we write~$\mathbf{s_1} \oplus \mathbf{s_2}$
(resp., $\mathbf{s_1} \ominus \mathbf{s_2}$), we will always assume
that~$\mathbf{s_1} \cap \mathbf{s_2} = \emptyset$ (resp., that~$\mathbf{s_2}
\subseteq \mathbf{s_1}$), i.e., that the operation is well-defined.  


\begin{lemma}
\label{lem:mobius_oplus_ominus}
    Let~$\mathbf{s_1},\mathbf{s_2}$ be two configurations such that~$\mathbf{s_1}
\oplus \mathbf{s_2}$ (resp., $\mathbf{s_1} \ominus \mathbf{s_2}$) is well-defined,
and let~$\mathbf{s}$ be the resulting configuration.  Then we
have~$\mu_\mathbf{s} = \mu_\mathbf{s_1} + \mu_\mathbf{s_2}$ (resp.,
$\mu_\mathbf{s} = \mu_\mathbf{s_1} - \mu_\mathbf{s_2}$).
\end{lemma}
\begin{proof}
    It is routine to show by bottom-up induction that for all~$n\in 2^{[k]}$ we
indeed have $\mu_\mathbf{s}(n) = \mu_\mathbf{s_1}(n) + \mu_\mathbf{s_2}(n)$
(resp., $\mu_\mathbf{s}(n) = \mu_\mathbf{s_1}(n) - \mu_\mathbf{s_2}(n)$).
\end{proof}

\begin{example}
\label{expl:0010001000100010}
    Consider the configuration~$\mathbf{s}$ from
Example~\ref{expl:0111001001110010} and let~$\mathbf{s_1}$ be the
configuration~$\{ \{1\}, \{1,2\}, \{1,3\}, \{1,2,3\} \}$ and~$\mathbf{s_2}$ be
the configuration~$\{ \{0\}, \{0,1\}, \{0,3\}, \{0,1,3\} \}$ (depicted in
Figures~\ref{fig:0010001000100010} and~\ref{fig:0101000001010000}). Then we
have~$\mathbf{s} = \mathbf{s_1} \oplus \mathbf{s_2}$. Moreover, one can check
that we indeed have~$\mu_\mathbf{s} = \mu_\mathbf{s_1} + \mu_\mathbf{s_2}$.
\end{example}

\begin{figure}
\begin{center}
\input{figures/0010001000100010.tex}
\caption{The configuration~$\mathbf{s_1}$ from
Example~\ref{expl:0010001000100010} and its associated Möbius
function~$\mu_\mathbf{s_1}$.}
\label{fig:0010001000100010}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\input{figures/0101000001010000.tex}
\caption{The configuration~$\mathbf{s_2}$ from
Example~\ref{expl:0010001000100010} and its associated Möbius
function~$\mu_\mathbf{s_2}$.}
\label{fig:0101000001010000}
\end{center}
\end{figure}


\paragraph*{Cancellation-freeness.}
We now impose another restriction on how we can combine configurations
using~$\oplus$ and~$\ominus$:

\begin{definition}
    Let~$\mathbf{s_1},\mathbf{s_2}$ be two configurations. We say that the
operation~$\mathbf{s_1} \oplus \mathbf{s_2}$ (resp., $\mathbf{s_1} \ominus
  \mathbf{s_2}$) is \emph{cancellation-free} when the operation is well-defined
  (i.e., it is a disjoint union or subset complement) and when, letting~$\mathbf{s}$ be the resulting
  configuration, we have $|\mu_\mathbf{s}| = |\mu_\mathbf{s_1}| + |\mu_\mathbf{s_2}|$.
\end{definition}

Note that Lemma~\ref{lem:mobius_oplus_ominus} and the triangle inequality already implied that we have $|\mu_\mathbf{s}| \leq
|\mu_\mathbf{s_1}| + |\mu_\mathbf{s_2}|$ when the operation is well-defined.
Moreover, observe that this definition is equivalent to saying that, for all $n \in 2^{[k]}$, we have $|\mu_\mathbf{s}(n)| =
|\mu_\mathbf{s_1}(n)| + |\mu_\mathbf{s_2}(n)|$, or equivalently that $\mu_\mathbf{s_1}(n)$
and $\mu_\mathbf{s_2}(n)$ have the same sign (or at least one of them is~$0$); hence
the name \emph{cancellation-free}.

\begin{example}
\label{expl:legality}
Continuing Example~\ref{expl:0010001000100010}, one can check that~$\mathbf{s_1}
  \oplus \mathbf{s_2}$ is cancellation-free.
\end{example}

\paragraph*{Cones and reachability.}
Our goal is to understand which configurations can be constructed using
by cancellation-free~$\oplus$ and~$\ominus$ operations, from some basic configurations that we call
\emph{cones}.  For each $n \in 2^{[k]}$, the \emph{cone} $C_n$ spanned by~$n$ is
simply the configuration of all supersets of~$n$, that is, $C_n \defeq\{n' \in 2^{[k]} |
n \subseteq n'\}$.

\begin{remark}
    Observe that the Möbius function of a cone~$C_n$ is simply
$\mu_{C_n}(n) = 1$ and $\mu_{C_n}(n') = 0$ for all $n' \neq n$.
  So $|\mu_{C_n}| = 1$, and in fact cones are precisely the configurations
  whose Möbius function has norm equal to~$1$.
\end{remark}

The set~$\R_k$ of \emph{reachable configurations} is then defined as the set of
all configurations that can be achieved by cancellation-free~$\oplus$ and~$\ominus$
operations from cones. Formally, it is the (unique) set of configurations which
is minimal by inclusion, contains the empty configuration and all cones, and is
closed under cancellation-free~$\oplus$ and~$\ominus$, i.e., 
    if~$\mathbf{s_1}$ and~$\mathbf{s_2}$ are in~$\R_k$ and
if~$\mathbf{s_1} \oplus \mathbf{s_2}$ (resp., $\mathbf{s_1} \ominus
\mathbf{s_2}$) is cancellation-free then the resulting configuration is in~$\R_k$.

\begin{example}
\label{expl:reachable}
    Continuing Example~\ref{expl:legality}, one can easily check
that~$\mathbf{s_1} = C_{\{2\}} \ominus C_{\{0,1\}}$ is cancellation-free, and that
$\mathbf{s_1} = C_{\{0\}} \ominus C_{\{0,2\}}$ is also cancellation-free;
hence~$\mathbf{s_1}$ and~$\mathbf{s_2}$ are reachable. Since~$\mathbf{s_1}
\oplus \mathbf{s_2}$ is also cancellation-free, we have that $\mathbf{s} = \mathbf{s_1}
\oplus \mathbf{s_2}$ is also reachable.
\end{example}

\paragraph{Problem statement.}
Our goal is to characterize which configurations are reachable in this sense. As
we will see, this is in fact not the case of all configurations. Specifically,
we want to show that all 
\emph{monotone} configurations are reachable. A configuration~$\mathbf{s}$ is
\emph{monotone} when for all~$n,n' \in 2^{[k]}$, if~$n \in \mathbf{s}$ and~$n
\subseteq n'$ then~$n' \in \mathbf{s}$.

\begin{conjecture}
\label{conj:monotone-reachable}
    All monotone configurations are reachable.
\end{conjecture}

\paragraph{Cone-reachability.}
A related problem, leading to a stronger conjecture on monotone functions,
concerns \emph{cone-reachability}. Intuitively, the set $\R'_k$ of
\emph{cone-reachable configurations} is the set of configurations that can be
reached when imposing that the second operand to an operation is always a cone.
Formally, it contains the empty configuration, contains all cones, and whenever
$\mathbf{s} \in \R'_k$ then for any cone $C_n$, if $\mathbf{s} \oplus C_n$ is
cancellation-free then it belongs to~$\R'_k$, and the same holds for $\mathbf{s}
\ominus C_n$.

A stronger conjecture than the above is:

\begin{conjecture}
\label{conj:monotone-conereachable}
    All monotone configurations are cone-reachable.
\end{conjecture}

We point out here that a variant this problem on arbitrary partial orders (instead of simply the Boolean powerset) has been presented in \url{https://cstheory.stackexchange.com/q/45679/38111}, where a generalization of Conjecture~\ref{conj:monotone-conereachable} has been stated for \emph{join semi-lattices}.
