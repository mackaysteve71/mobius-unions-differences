\documentclass[a4paper]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage[appendix=inline]{apxproof}
\usepackage{relsize}
\usepackage{stackengine}
\stackMath
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{todonotes}
\usepackage{hyperref}
\hypersetup{
	    colorlinks,
	    linkcolor={red!50!black},
	    citecolor={blue!50!black},
	    urlcolor={blue!30!black},
	    breaklinks=true
	    }
\usepackage{cite}
\usepackage{mathabx}
\newtheoremrep{theorem}{Theorem}[section]
\newtheorem{openpb}[theorem]{Open problem}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{remark}[theorem]{Remark}
\newtheoremrep{proposition}[theorem]{Proposition}
\newtheoremrep{lemma}[theorem]{Lemma}
\newtheoremrep{example}[theorem]{Example}
\newtheoremrep{definition}[theorem]{Definition}

\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\NN}{\mathbb{N}}

\input{macros}


\begin{document}

\title{Which Sets Can be Expressed as Disjoint Union and Subset Complement
Without Möbius Cancellations?}
\author{Antoine Amarilli\\{\small \url{https://a3nm.net/}}\\{\small
\texttt{a3nm@a3nm.net}} \and Louis
Jachiet\\{\small \url{https://louis.jachiet.com/}}\\{\small
\texttt{louis@jachiet.com}} \and Mikaël Monet\\{\small
\url{http://mikael-monet.net/}}\\{\small \texttt{mikael.monet@imfd.cl}}}
\date{}
\maketitle

\begin{abstract}
  This note presents a combinatorial problem on the Boolean lattice asking about
  which sets of subsets (called \emph{configurations}) can be achieved from the
  monotone subsets with only one minimal element (called \emph{cones}) using
  the operations of disjoint union and subset complement without so-called Möbius
  cancellations, i.e., while ensuring that the Möbius functions of the
  configurations being combined do not lead to cancellations.

  The problem is motivated by the so-called \emph{intensional-extensional
  conjecture} on query evaluation on probabilistic tuple-independent databases,
  and whether unions of conjunctive queries that are tractable for this problem
  admit lineage representations in a tractable circuit class:
  see~\cite{monet2020solving} for details. We believe that solving the problem
  presented in this note would imply a solution to this conjecture. However,
  this note only presents the combinatorial problem: it does not present the
  connection to this conjecture, and assumes no familiarity with database theory
  or circuit classes.
\end{abstract}


\section{Preliminaries}
\label{sec:prelims}
\input{preliminaries}

\section{Problem Statement}
\label{sec:game}
\input{game}

\section{Alternative Problem Statements}

Let us consider the Boolean lattice over Boolean events $E_1, \ldots,
E_n$, and let us consider an event $S = \bigcup_j C_j$, where each
$C_j$ is a \emph{basic conjunction}, i.e., a conjunction of some of
the events~$E_i$.

An alternative way to phrase our problem is to ask whether $S$ can be
expressed as a function
of basic conjunctions (not necessarily the ones used in the definition of~$S$) using only the disjoint union operator ($T = T_1
\cup T_2$ where $T_1$ and $T_2$ are disjoint) and the subset
complement operator ($T = T_1 \setminus T_2$ where $T_2 \subseteq
T_1$).

In general, it is always possible to built $S$ with disjoint union and subset
complement by applying recursively the inclusion-exclusion principle on the
definition of~$S$.
So the question that we ask is whether we can express~$S$ in the sense above but
satisfying a \emph{minimality requirement}: each basic conjunction $C_j$ must
be used exactly as specified by its coefficient in the Möbius function associated
to the configuration corresponding to~$S$, i.e., with the number of
times indicated by the absolute value, and the polarity indicated by
the sign of the Möbius function. This means in particular that conjunctions
with Möbius coefficient zero cannot be used: these are the conjunctions that
can be canceled out in the inclusion-exclusion formula.

An equivalent way to see this is to take the expression of $S$ as a function
of basic conjunctions given by the inclusion-exclusion formula, apply
cancellations across terms so that the coefficient of each basic
conjunction is its value in the Möbius function, and ask about whether
these terms can be ordered such that additions correspond to disjoint
unions and subtractions correspond to subset complements.

Alternatively, we ask whether $S$, seen as a monotone DNF Boolean
function, can be expressed as a function of basic conjunctions
(monotone conjunctions of variables) using disjoint union and
negation, i.e., as a d-D circuit, as a function of basic conjunctions,
each of which being used with the right polarity and cardinality. This
connects back to the motivation of our problem, i.e., the
intensional-extensional conjecture on probabilistic
databases~\cite{monet2020solving}.


\section{Current Results}
\label{sec:know}
\input{know}


\bibliographystyle{apalike}
\bibliography{main}

\end{document}
