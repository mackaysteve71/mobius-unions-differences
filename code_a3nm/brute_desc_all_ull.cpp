#include<cstdio>
#include<array>
#include<cassert>
#include<vector>
#include<unordered_set>
#include<algorithm>

/* Explore the Boolean lattice over N elements, test configurations (Boolean
 * functions) over the lattice, and find the ones that are "irreducible", i.e.,
 * cannot be expressed as a disjoint union or subset difference in a way that
 * splits properly the values of the Mobius function.
 *
 * Basic usage: set the defines below, compile, and run.
 *
 * You can also do ./a.out CONFIG to test CONFIG and display info about it (or
 * about its minified version to break symmetries, including info about which
 * decomposition was found if any), or ./a.out CONFIG skip to do the same but
 * displaying only basic info (no testing).
 *
 * Beware, all configurations (input or output) are written as binary words with
 * the least significant bit (corresponding to the status of the empty set of
 * the lattice to the LEFT) */

using namespace std;

#define N 5

// also check if decomposable with cones (slower)
// this is the special case where one of the operands must be one of the basic
// "cones"
#define SEARCH_CONES 0

// if set to != 0, will restrict the search to values of that cardinality
#define ONLY_WITH_CARDINALITY 0

// if set, will restrict the search to symmetric configurations
#define ONLY_SYMMETRIC 1
// if set, will restrict the search to antisymmetric configurations
#define ONLY_ANTISYMMETRIC 0

// if set, will skip non-minimal values (and display info about the minimal
// version of provided configurations, when an argument is given to the program)
#define ONLY_MINIMAL 1

// if set, will do in decreasing order (ignored for ONLY_SYMMETRIC)
#define ORDER_DESC 0

typedef unsigned long long ull;
typedef array<char, ((ull) 1) << N> vect;

/* Utility functions */

// print a binary value of n bits (beware, LSB is left)
void print_val(ull val, unsigned int n) {
  for (unsigned int i = 0; i < n; i++) {
    printf("%lld", val%2);
    val = val >> 1;
  }
  printf("\n");
}

// reverse binary value of n bits
ull reverse(ull x, ull n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    v = v | (x & 1);
    x = x >> 1;
  }
  return v;
}

ull read_config(char *s, unsigned int n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    if (s[i] == '1')
      v += 1;
  }
  return reverse(v, n);
}

// compute complement of value of n bits
inline ull comp(ull x, int n) {
  return ((((ull) 1) << n)-1) & (~x);
}

/* Basic computations */

// compute euler characteristic of configuration x
int euler(ull x) {
  int v = 0;
  for (unsigned int i = 0; i < (((ull) 1) << N); i++) {
    int add = (((__builtin_popcount(i) % 2) == 0) ? 1 : -1) * ((x & (((ull) 1) << i)) ? 1 : 0);
    v += add;
  }
  return v;
}

// write mobius value of x in vect v
void mobius(ull x, vect &v) {
  for (unsigned int i = 0; i < (((ull) 1) << N); i++) {
    int s = 0;
    for (unsigned int j = 0; j < i; j++) {
      if ((j & i) != j)
        continue; // not subset
      // we have j subset of i
      s += v[j];
    }
    // value for i is 1 or 0 depending on whether i is lit, minus the sum
    v[i] = ((x & (((ull) 1) << i)) ? 1 : 0) - s;
  }
}

// is a configuration monotone?
bool monotone (ull conf) {
  for (unsigned int i = 0; i < (((ull) 1) << N); i++) {
    for (unsigned int j = 0; j < N; j++) {
      if ((conf & (((ull) 1) << i)) && !(conf & ((((ull) 1) << (i | (((ull) 1) << j))))))
        return false;
    }
  }
  return true;
}

// get the minimal configuration of conf under permutations to the base elements
// of the lattice
ull get_minimal (ull conf) {
  vector<int> P;
  ull ans = conf;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    ull conf2 = 0;
    for (unsigned int p = 0; p < (((ull) 1) << N); p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf & (((ull) 1) << p))
        conf2 = conf2 | (((ull) 1) << np);
    }
    if (conf2 < ans)
      ans = conf2;
  } while (next_permutation(P.begin(), P.end()));
  return ans;
}

// test if conf is minimal in the sense of the get_minimal function
bool minimal (ull conf) {
  vector<int> P;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    ull conf2 = 0;
    for (unsigned int p = 0; p < (((ull) 1) << N); p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf & (((ull) 1) << p))
        conf2 = conf2 | (((ull) 1) << np);
    }
    if (conf2 < conf)
      return false; // faster to exit
  } while (next_permutation(P.begin(), P.end()));
  return true;
}

// fill cones with the set of basic elements (cones)
void prepare_cones (unordered_set<ull> &cones) {
  for (ull i = 0; i < (((ull) 1) << N); i++) {
    ull c = 0;
    for (ull j = i; j < (((ull) 1) << N); j++) {
      if ((j & i) == i)
        c |= (((ull) 1) << j); // j is a superset of i;
    }
    cones.insert(c);
  }
}

// check if config is a cone
bool is_cone(ull config) {
  // prepare the cones
  unordered_set<ull> cones;
  prepare_cones(cones);
  return (cones.find(config) != cones.end());
}

// print value of n bits together with all the info
void print_val_stat(ull val, unsigned int n, bool plus_dec, bool plus_cone_dec, bool minus_dec, bool minus_cone_dec, int skip) {
  ull nval = val;
  for (unsigned int i = 0; i < n; i++) {
    printf("%lld", nval%2);
    nval = nval >> 1;
  }
  printf(" ");
  if (ONLY_MINIMAL && skip == 2) {
    // skipped for monotonicity
    printf("skipped -- not minimal\n");
    return;
  }
  printf("euler:%d ", euler(val));
  printf("hamming:%d ", __builtin_popcount(val & ((((ull)1) << (((ull) 1) << N))-1)));
  if (!skip) {
    if (!plus_dec && !minus_dec)
      printf("irreductible! ");
    if (SEARCH_CONES && !plus_cone_dec && !minus_cone_dec)
      printf("cone_irred! ");
  }
  if (monotone(val))
    printf("MONOTONE ");
  if (is_cone(val))
    printf("ISCONE ");
  printf("\n");
}

/* Main test: check for decomposability and display all the info */

void test (ull crc, bool detail) {
  if (!crc)
    return;

  // prepare the cones
  unordered_set<ull> cones;
  prepare_cones(cones);

  bool plus_dec = false;
  bool plus_cone_dec = false;
  bool minus_dec = false;
  bool minus_cone_dec = false;

  if (ONLY_MINIMAL && !minimal(crc)) {
    print_val_stat(crc, (((ull) 1) << N), plus_dec, plus_cone_dec, minus_dec, minus_cone_dec, 2);
    return;
  }

  // test if decomposable by a +
  ull v = 0;
  while (true) {
    v |= comp(crc, ((ull) 1) << N);
    v += 1;
    v &= crc;
    // v iterates over subsets
    ull v2 = crc - v;
    // v2 is the complement
    // only check when v!=0 and v2!=0
    if (v && v2) {
      assert ((v + v2) == crc);
      assert ((v | v2) == crc);
      vect m, m2;
      mobius(v, m);
      mobius(v2, m2);
      // check that the combination is legal
      bool ok = true;
      for (unsigned int i = 0; i < (((ull) 1) << N); i++) {
        if (abs(m[i] + m2[i]) != abs(m[i]) + abs(m2[i])) {
          ok = false; // illegal
          break;
        }
      }
      if (ok) {
        plus_dec = true; //plus_decomposable
        if (detail) {
          printf("Plus decomposable\n");
          print_val(v, ((ull) 1) << N);
          print_val(v2, ((ull) 1) << N);
        }
        if (SEARCH_CONES) {
          if (cones.find(v) != cones.end() || cones.find(v2) != cones.end()) {
            plus_cone_dec = true;
            break; // now we know everything
          }
        } else {
          break; // we know plus_decomposable
        }
      }
      //print_val(v, 16);
    }
    if (v == crc)
      break; // done looping
  }

  // test if decomposable by a -
  v = 0;
  ull ncrc = comp(crc, ((ull) 1) << N);
  while (true) {
    v |= (~ncrc);
    v += 1;
    v &= ncrc;
    // v is a subset of the complement
    ull v2 = crc | v;
    assert(v2 - v == crc);
    if (v) {
      vect m, m2;
      mobius(v, m);
      mobius(v2, m2);
      // check that legal
      bool ok = true;
      for (unsigned int i = 0; i < (((ull) 1) << N); i++) {
        if (abs(m[i] - m2[i]) != abs(m[i]) + abs(m2[i])) {
          ok = false;
          break;
        }
      }
      if (ok) {
        minus_dec = true;
        if (detail) {
          printf("Minus decomposable\n");
          print_val(v2, ((ull) 1) << N);
          print_val(v, ((ull) 1) << N);
        }
        if (SEARCH_CONES) {
          if (cones.find(v) != cones.end() || cones.find(v2) != cones.end()) {
            minus_cone_dec = true;
            break; // now we know everything
          }
        } else {
          break; // we know plus_decomposable
        }
        break;
      }
    }
    if (v == ncrc)
      break;
  }

  print_val_stat(crc, (((ull) 1) << N), plus_dec, plus_cone_dec, minus_dec, minus_cone_dec, false);
}

int main (int argc, char **argv) {
  if (argc == 3) {
    // case of ./a.out CONFIG skip: display info without computations
    ull crc = read_config(argv[1], ((ull) 1) << N);
    if (ONLY_MINIMAL) {
      // display info about the minimal configuration
      print_val_stat(get_minimal(crc), (((ull) 1) << N), false, false, false, false, true);
    } else {
      print_val_stat(crc, (((ull) 1) << N), false, false, false, false, true);
    }
  } else if (argc == 2) {
    // case of ./a.out CONFIG: display info with computations
    ull crc = read_config(argv[1], ((ull) 1) << N);
    if (ONLY_MINIMAL) {
      test(get_minimal(crc), true); 
    } else {
      test(crc, true); 
    }
  } else {
    // basic case: test all configurations
    if (ONLY_SYMMETRIC || ONLY_ANTISYMMETRIC) {
      // iterate over all symmetric configurations of the poset with N elements
      // => generate the first half
      for (ull c = 0; c < ((ull) 1) << (((ull) 1) << (N-1)); c++) {
        // check it lights exactly the requested number of vertices
        if (ONLY_WITH_CARDINALITY && (__builtin_popcount(c) != ONLY_WITH_CARDINALITY/2))
          continue;
        // glue it to its reverse
        ull crc;
        assert(!ONLY_SYMMETRIC || !ONLY_ANTISYMMETRIC);
        if (ONLY_SYMMETRIC)
          crc = c | (reverse(c, ((ull) 1) << (N-1)) << (((ull) 1) << (N-1)));
        if (ONLY_ANTISYMMETRIC)
          crc = c | (comp(reverse(c, ((ull) 1) << (N-1)), ((ull) 1) << (N-1)) << (((ull) 1) << (N-1)));

        test(crc, false);
      }
    } else {
      if (ORDER_DESC) {
        for (ull c = (((ull) 1) << (((ull) 1) << N)) - 1; c >= 0; c--) {
          // check it lights exactly 2^{N-2} vertices
          if (ONLY_WITH_CARDINALITY && (__builtin_popcount(c) != ONLY_WITH_CARDINALITY))
            continue;
          test(c, false);
        }
      } else {
        for (ull c = 0; c < (((ull) 1) << (((ull) 1) << N)); c++) {
          // check it lights exactly 2^{N-2} vertices
          if (ONLY_WITH_CARDINALITY && (__builtin_popcount(c) != ONLY_WITH_CARDINALITY))
            continue;
          test(c, false);
        }
      }
    }
  }

  return 0;

}
