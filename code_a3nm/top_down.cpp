#include<cstdio>
#include<unordered_map>
#include<unordered_set>
#include<map>
#include<cassert>
#include<vector>
#include<algorithm>

// bruteforce to decompose a configuration into elementary configurations

#ifndef N
#warning "N should have been defined on the command line"
#define N 3
#endif
#define M (((ull) 1) << N)

// if 1, play the cone game (the RHS of any operation must be a cone)
#ifndef ONLY_CONES
#define ONLY_CONES 0
#endif

// do not test reachability but only irreducibility
#ifndef TEST_IRRED
#define TEST_IRRED 0
#endif

// whether to display explanations
#ifndef EXPLAIN
#define EXPLAIN 1
#endif

// whether to test all configurations (instead of those provided on stdin)
#ifndef ALL
#define ALL 0
#endif

// if ALL=1, only test...
#ifndef ONLY_MONOTONE
#define ONLY_MONOTONE 0
#endif

// only minimal configurations (for ALL=1), make input configuration minimal
// (for ALL=0)
#ifndef ONLY_MINIMAL
#define ONLY_MINIMAL 1
#endif

// smart way to prune exploration of cones, seems to work on N=5
#ifndef EXPERIMENTAL_PRUNE_CONES
#define EXPERIMENTAL_PRUNE_CONES 0
#endif

#if N==7
typedef unsigned __int128 ull;
#else
typedef unsigned long long ull;
#endif

// interval to print progress
#ifndef STEP
#define STEP 100000000
#endif

// size after which to drop cache
#ifndef CLEAR_EVERY
#define CLEAR_EVERY 1000000
#endif

// max
// https://oeis.org/A003182/internal
ull OEIS[] = {2, 3, 5, 10, 30, 210, 16353, 490013148};

using namespace std;

typedef array<char, M> vect;

// global cache of which configurations were reached
unordered_map<ull, bool> reachable_cache;
// global cache of the left operand, right operand, operation to reach a config
unordered_map<ull, ull> reach_l, reach_r;
unordered_map<ull, char> reach_op;

// print a configuration
void print_val(ull val, unsigned int n) {
  for (unsigned int i = 0; i < n; i++) {
    printf("%llu", val%2);
    val = val >> 1;
  }
}

// reverse binary value of n bits
ull myreverse(ull x, ull n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    v = v | (x & 1);
    x = x >> 1;
  }
  return v;
}

// print a mobius vector
void print_vect(vect v) {
  for (int i = 0; i < M; i++)
    printf("%d:%d ", i, v[i]);
  printf("\n");
}

// weight of a mobius vector
int weight(vect v) {
  int w = 0;
  for (int i = 0; i < M; i++)
    w += v[i];
  return w;
}

// absolute weight of a mobius vector
int absweight(vect v) {
  int w = 0;
  for (int i = 0; i < M; i++)
    w += abs(v[i]);
  return w;
}

// sign of a number
inline int sgn(int x) {
  if (x < 0) return -1;
  if (x > 0) return 1;
  return 0;
}

// fill the table of how many pluses and minuses are remaining below each cell
void prepare_remaining(vect v, vect &remaining_pluses, vect &remaining_minuses) {
  for (int i = 0; i < M; i++) {
    int plus = 0, minus = 0;
    for (int j = 0; j <= i; j++)
      if ((i | j) == i) // j \subseteq i
        if (v[j] > 0)
          plus += v[j];
        else
          minus += -v[j]; // also covers 0 in which case nothing happens
    remaining_pluses[i] = plus;
    remaining_minuses[i] = minus;
    //printf("INIT rp %d rm %d v:%d at %d\n", remaining_pluses[i], remaining_minuses[i], v[i], i);
  }
}

void apply_nullify(vect v, int i, int f, vect &remaining_pluses, vect &remaining_minuses) {
  int w = v[i];
  //printf("remove %d to all vertices above %d\n", f*w, i);
  for (int j = i; j < M; j++)
    if ((i | j) == j) {
      //printf("before rp %d rm %d in %d\n", remaining_pluses[j], remaining_minuses[j], j);
      if (w > 0)
        remaining_pluses[j] -= f*w;
      else
        remaining_minuses[j] -= f*abs(w);
      //printf("now rp %d rm %d in %d\n", remaining_pluses[j], remaining_minuses[j], j);
    }
}

// take into account that we made our choice for cell i
// so edit remaining_pluses and remaining_minuses to get rid of those of cell i
void nullify(vect v, int i, vect &remaining_pluses, vect &remaining_minuses) {
  apply_nullify(v, i, 1, remaining_pluses, remaining_minuses);
}
// undo nullify when backtracking
void unnullify(vect v, int i, vect &remaining_pluses, vect &remaining_minuses) {
  apply_nullify(v, i, -1, remaining_pluses, remaining_minuses);
}

// The vector clr stores the "weights assigned below each cell" (for the left or right)
// This function takes into account that we have added weight w to cell i on the
// side of the vector clr.
// So we must propagate in the cells above i in clr this new weight
// While doing so, we compute for each cell above its minimal and maximal
// weight, to identify a problem.
// If we are going to be stuck, still perform the changes in the whole vector,
// so that we can undo them more easily.
// coef swaps the pluses and minuses when doing a difference
bool add_lr(vect v, int i, int w, vect &remaining_pluses, vect &remaining_minuses, vect &clr, bool special, bool side, bool enablecheck, int coef) {
  // in special mode, for the game with cones, check that the function of
  // everything to the left and right is OK
  // side=false: left side, we take everything
  // side=right: right side, we take nothing
  // only if enablecheck
  bool ok = true;
  for (int j = i; j < M; j++)
    if ((i | j) == j) {
      clr[j] += w;
      int mn = clr[j] - ((coef == 1) ? remaining_minuses : remaining_pluses)[j];
      int mx = clr[j] + ((coef == 1) ? remaining_pluses : remaining_minuses)[j];
      if (mn > 1 || mx < 0) {
        // at cell j, we will not be able to assign weights in the cells below j
        // (including j) so as to give it a weight in {0, 1}
        //printf("clr[%d] is %d but sum of rp %d and rm %d is %d\n", j, clr[j], remaining_pluses[j], remaining_minuses[j] ,remaining_pluses[j] - remaining_minuses[j]);
        ok = false;
      }
      if (EXPERIMENTAL_PRUNE_CONES && enablecheck && ONLY_CONES && special && j > i) {
        if (!side) {
          int nv = clr[j] + ((coef == 1) ? remaining_pluses : remaining_minuses)[j] - ((coef == 1) ? remaining_minuses : remaining_pluses)[j];
          if (nv != 0 && nv != 1) {
            //printf("clr[%d] is %d but sum of rp %d and rm %d is %d\n", j, clr[j], remaining_pluses[j], remaining_minuses[j] ,remaining_pluses[j] - remaining_minuses[j]);
            //printf("BAD at %d\n", j);
            ok = false;
          }
        } else {
          if (clr[j] != 0 && clr[j] != 1) {
            //printf("clr[%d] is %d\n", j, clr[j]);
            ok = false;
          }
        }
      }
    }
  return ok;
}

// converts a vector to a config
ull vect_to_config(const vect v) {
  ull c = 0;
  for (int i = 0; i < M; i++) {
    int w = 0;
    for (int j = 0; j <= i; j++)
      if ((i | j) == i) // j \subseteq i
        w += v[j];
    assert(w == 0 || w == 1); // the configuration should be acceptable
    if (w == 1)
      c = c | (((ull) 1) << i);
  }
  return c;
}

// computes the vector of config x and stores it in v
void config_to_vect(ull x, vect &v) {
  for (unsigned int i = 0; i < M; i++) {
    int s = 0;
    for (unsigned int j = 0; j < i; j++) {
      if ((j & i) != j)
        continue; // not subset
      // we have j subset of i
      s += v[j];
    }
    // value for i is 1 or 0 depending on whether i is lit, minus the sum
    v[i] = ((x & (((ull) 1) << i)) ? 1 : 0) - s;
  }
}

// pre-declare reachable (mutually recursive functions)
bool reachable(vect);

// try to split the weights of vector v, at cell i
// - sgnop is the operand (+ for union, - for difference)
// - remaining_pluses and remaining_minuses store the number of remaining pluses
// and minuses below each cell (for the unaffected weights in v after cell i
// inclusive)
// - current0 current1 is the vector of the currently build left and right
// configuration (up to cell i exclusive)
// - clr0 clr1 indicates the current total weight below each cell given what was
// assigned up to cell i exclusive (to detect impossibilities)
// - one_right stores if we have already assigned something to the right (useful
// only for ONLY_CONES)
bool partition_split(const vect v, int i, int sgnop, vect &remaining_pluses, vect &remaining_minuses, vect &current0, vect &current1, vect &clr0, vect &clr1, bool one_right) {
  //printf("try split %d for sign %d\n", i, sgnop);
  if (i == M) {
    // we are done partitioning
    ull config = vect_to_config(v);
    if (!absweight(current0) || !absweight(current1)) {
      return false; // trivial partition
    }
    // now recursively test if the operands are reachable
    // (or if TEST_IRRED is set we don't care about the next level)
    bool reach0 = reachable(current0);
    bool reach1 = reachable(current1);
    /*if (!reach0 || !reach1) {
      printf("BACKTRACK on a weight %d = %d+%d (reach %d %d)!!!\n", absweight(v), absweight(current0), absweight(current1), reach0?1:0, reach1?1:0);
    }*/
    //assert(reach0 && reach1);
    if (TEST_IRRED || (reach0 && reach1)) {
      if (EXPLAIN) {
        // store the operands
        reach_op[config] = sgnop;
        reach_l[config] = vect_to_config(current0);
        reach_r[config] = vect_to_config(current1);
        assert(reach_l[config] != (ull) 0);
      }
      return true;
    } else {
      return false; // this partition does not work, one of the operands is unreachable
    }
  }
  // ok let's try to split the weights at cell i
  if (v[i] == 0) {
    // no weight to split, let's only make sure that the weights are legal
    if (clr0[i] != 0 && clr0[i] != 1)
      assert(false);
    if (clr1[i] != 0 && clr1[i] != 1)
      assert(false);
    // ok that's fine, so go to the next cell
    return partition_split(v, i+1, sgnop, remaining_pluses, remaining_minuses, current0, current1, clr0, clr1, one_right);
  }
  // we have weight to split at cell i, what are the possible values?
  for (int j = 0; j <= abs(v[i]); j++) {
    // note that only two j in this loop can give to a recursive call
    //
    int goes_left = j*sgn(v[i]); // weight that goes left: j with the right sign
    int goes_right = sgnop*(v[i] - goes_left); // weight that goes right: remaining weight, but depends on sgnop

    if (ONLY_CONES && one_right && goes_right)
      continue; // only cones, so we only put one thing to the right

    // sanity checks
    assert(abs(goes_left) + abs(goes_right) == abs(v[i]));
    assert(goes_left + sgnop*goes_right == v[i]);

    // this will be our final cumulative values to the right and left at cell i
    // we don't need to store them, only to check them
    int vlr_left = clr0[i] + goes_left;
    int vlr_right = clr1[i] + goes_right;
    if (vlr_left != 0 && vlr_left != 1) {
      continue; // this weight assignment gives an unacceptable configuration to the left
    } else if (vlr_right != 0 && vlr_right != 1) {
      continue; // this weight assignment gives an unacceptable configuration to the right
    }
    //printf("try weight %d %d at %d op %d\n", goes_left, goes_right, i, sgnop);

    // ok, it is acceptable to split the weight in goes_left and goes_right
    // let's try it
    // first let's clear remaining_pluses and remaining_minuses of the weight
    nullify(v, i, remaining_pluses, remaining_minuses);
    bool reasonable = true; // check that the split has a chance of working
    // assign the weights
    current0[i] = goes_left;
    current1[i] = goes_right;
    // try to add the weight to the cells above i in clr0 and clr1
    if (!add_lr(v, i, current0[i], remaining_pluses, remaining_minuses, clr0, goes_right != 0, false, true, 1)) {
      reasonable = false; // our weight assignment will necessarily break some cell left
      //printf("not reasonable left\n");
    }
    if (!add_lr(v, i, current1[i], remaining_pluses, remaining_minuses, clr1, goes_right != 0, true, true, (sgnop==1) ? 1 : -1 )) {
      reasonable = false; // our weight assignment will necessarily break some cell right
      //printf("not reasonable right\n");
    }
    if (reasonable) { // we can try to continue the recursive split
      //printf("actually reasonable\n");
      reasonable = partition_split(v, i+1, sgnop, remaining_pluses,
          remaining_minuses, current0, current1, clr0, clr1, 
          one_right || (goes_right != 0)); // store info of if we have put something right
    }
    // now undo our changes
    // no need to undo the changes in current0
    // reset clr0 and clr1
    add_lr(v, i, -current0[i], remaining_pluses, remaining_minuses, clr0, goes_right != 0, false, false, 1);
    add_lr(v, i, -current1[i], remaining_pluses, remaining_minuses, clr1, goes_right != 0, true, false, (sgnop==1) ? 1 : -1);
    // undo the changes to remaining_pluses and remaining_minuses
    unnullify(v, i, remaining_pluses, remaining_minuses);
    if (reasonable)
      return true; // yay! that split worked
  }
  return false; // noes! no split worked
}

// try to partition the mobius vector v, with operand sgnop (1 for +, -1 for -)
bool partition(const vect v, int sgnop) {
  //printf("enter partition %d\n", absweight(v));
  vect remaining_pluses, remaining_minuses;
  vect current[2]; // current situations to the left and right
  vect clr[2]; // current cumulative weights in the configuration left and right

  // initialize remaining_pluses and remaining_minuses
  prepare_remaining(v, remaining_pluses, remaining_minuses);
  // initialize current and clr
  for (int i = 0; i < M; i++)
    for (int b = 0; b <= 1; b++)
      current[b][i] = clr[b][i] = 0;

  // start the recursive split
  bool val = partition_split(v, 0, sgnop, remaining_pluses, remaining_minuses, current[0], current[1], clr[0], clr[1], false);

  return val;
}

// check if Möbius vector v is reachable
bool reachable(const vect v) {
  ull config = vect_to_config(v);
  /*printf("REACHABLE: ");
  print_val(config, M);
  printf("\n");
  print_vect(v);*/
  int w = absweight(v);
  if (w == 0)
    return true; // 0 vector
  if (w == 1 && weight(v) == 1)
    return true; // base cone

  // try the global cache
  unordered_map<ull,bool>::const_iterator it = reachable_cache.find(config);
  if (it != reachable_cache.end())
    return it->second; // already computed

  // try to partition with a sum or a difference
  bool val = (partition(v, 1) || partition(v, -1));

  // store in cache
  reachable_cache[config] = val;
  if (val && EXPLAIN) {
    assert(reach_l[vect_to_config(v)] != ((ull) 0));
  }
  return val;
}

// explain how to reach a configuration config, d is the depth for formatting
void explain(ull config, int d) {
  vect v;
  config_to_vect(config, v);
  if (absweight(v) == 0)
    return; // trivial 
  if (absweight(v) == 1) {
    // basic cone
    if (!ONLY_CONES)
      for (int i = 0; i < d; i++)
        printf("  "); // indentation
    print_val(config, M);
    printf(" is a basic cone\n", config);
    return;
  }
  if (!ONLY_CONES)
    for (int i = 0; i < d; i++)
      printf("  "); // indentation
  print_val(config, M);
  printf(" = ");
  assert(reach_l[config] != ((ull) 0));
  assert(reach_r[config] != ((ull) 0));
  print_val(reach_l[config], M);
  if (reach_op[config] == 1)
    printf(" + ");
  else
    printf(" - ");
  vect vr;
  config_to_vect(reach_r[config], vr);
  if (ONLY_CONES && absweight(vr) == 1) {
    // print cones nicely
    int mn = 0;
    for (int i = 0; i < M; i++)
      if (vr[i])
        mn = i;
    printf("CONE:");
    print_val(myreverse(mn, N), N);
  } else {
    print_val(reach_r[config], M);
  }
  printf("\n");
  if (!TEST_IRRED) {
    explain(reach_l[config], d+1);
    if (!ONLY_CONES)
      explain(reach_r[config], d+1);
  }
}

// check if config is monotone
bool monotone(ull conf) {
  for (unsigned int i = 0; i < M; i++) {
    for (unsigned int j = 0; j < N; j++) {
      if ((conf & (((ull) 1) << i)) && !(conf & ((((ull) 1) << (i | (1 << j))))))
        return false;
    }
  }
  return true;
}

// check if config is minimal
bool minimal(ull conf) {
  vector<int> P;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    ull conf2 = 0;
    for (unsigned int p = 0; p < M; p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf & (((ull) 1) << p))
        conf2 = conf2 | (((ull) 1) << np);
    }
    if (conf2 < conf)
      return false; // faster to exit
  } while (next_permutation(P.begin(), P.end()));
  return true;
}

// get the minimal configuration of conf under permutations to the base elements
// of the lattice
ull get_minimal(ull conf) {
  vector<int> P;
  ull ans = conf;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    ull conf2 = 0;
    for (unsigned int p = 0; p < (((ull) 1) << N); p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf & (((ull) 1) << p))
        conf2 = conf2 | (((ull) 1) << np);
    }
    if (conf2 < ans)
      ans = conf2;
  } while (next_permutation(P.begin(), P.end()));
  return ans;
}


ull read_config(char *s, unsigned int n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    if (s[i] == '1')
      v += 1;
  }
  return myreverse(v, n);
}

void test_input_configs() {
  ull count = 0;
  //unordered_set<ull> tested = {};
  while (true) {
    char buf[M + 3];
    int ret = scanf("%s", buf);
    if (ret != 1)
      break;
    vect v;
    ull read = read_config(buf, M);
    ull crc;
    if (ONLY_MINIMAL) {
      ull crc = get_minimal(read);
    } else {
      crc = read;
    }
//    if (!monotone(crc)) {
//      printf("ERROR: %llu is not monotone \n", crc);
//      print_val(crc, M);
//      printf("\n");
//      return 1;
//    }
//    if (!minimal(crc)) {
//      printf("ERROR: %llu is not minimal\n", crc);
//      print_val(crc, M);
//      printf("\n");
//      return 1;
//    }
    /*if (tested.find(crc) != tested.end()) {
      printf("DUPE %llu already tested\n", crc);
      print_val(crc, M);
      printf("\n");
      return;
    }
    tested.insert(crc);*/
    printf("####### CONSIDERING (%llu of %llu): ", count, OEIS[N]-2);
    print_val(crc, M);
    printf("\n");

    config_to_vect(crc, v);
    if (reachable(v)) {
      printf(" reached\n");
      if (EXPLAIN) 
        explain(crc, 0);
    } else {
      print_val(crc, M);
      if (TEST_IRRED)
        printf(" NOT REDUCTIBLE\n");
      else
        printf(" NOT REACHED\n");
      return;
    }
    count++;
  }
  printf("reached %llu minimal non-constant monotone functions out of %llu\n", count, OEIS[N]-2);
  return;
}

void test_all_monotone_configs() {
  vect v;
  unsigned long long count = 0;
  // do monotone functions, naively
  // this excludes the null function
  ull MX = 0;
  for (int p = 0; p < M; p++)
    MX |= ((ull) 1) << p;
  for (ull i = MX; i >= ((ull) 1) << (M-1); i--) {
    if (!(i % 100000000)) {
      fprintf(stderr, "%llu of %llu (%llu monotone seen)...\n", MX - i, ((ull) 1) << (M-1), count);
    }
    if (!monotone(i) || !minimal(i))
      continue;
    printf("####### CONSIDERING: ");
    print_val(i, M);
    printf("\n");

    config_to_vect(i, v);
    if (reachable(v)) {
      printf(" reached\n");
      if (EXPLAIN) 
        explain(i, 0);
    } else {
      print_val(i, M);
      if (TEST_IRRED)
        printf(" NOT REDUCTIBLE\n");
      else
        printf(" NOT REACHED\n");
      return;
    }
    count++;
  }
  printf("reached %llu minimal nonzero monotone functions out of %llu\n", count, OEIS[N]-1);
  return;
}

void test_all_configs() {
  vect v;
  unsigned long long count = 0;
  unsigned long long reached = 0, notreached = 0;
  // do monotone functions, naively
  // this excludes the null function
  ull MX = 0;
  for (int p = 0; p < M; p++)
    MX |= ((ull) 1) << p;
  for (ull i = 0; i <= MX; i++) {
    // hack
    if (i == 1 && ONLY_MONOTONE) {
      i = (((ull) 1) << (M-1)) - 1;
      continue;
    }
    if (!(i % STEP)) {
      fprintf(stderr, "At %llu of 1+%llu (%llu seen of which got %llu missed %llu)...\n", i, MX, count, reached, notreached);
    }
    if (ONLY_MONOTONE && !monotone(i))
      continue;
    if (ONLY_MINIMAL && !minimal(i))
      continue; // this test is not so fast
    /*printf("####### CONSIDERING: ");
    print_val(i, M);
    printf("\n");*/

    config_to_vect(i, v);
    if (reachable(v)) {
      reached++;
      if (EXPLAIN) {
        print_val(i, M);
        printf(" reached\n");
        explain(i, 0);
      }
    } else {
      notreached++;
      print_val(i, M);
      if (TEST_IRRED)
        printf(" NOT REDUCTIBLE\n");
      else
        printf(" NOT REACHED\n");
    }
    count++;
    if (reachable_cache.size() > CLEAR_EVERY) {
      fprintf(stderr, "clearing caches\n");
      reachable_cache.clear();
      reach_l.clear();
      reach_r.clear();
      reach_op.clear();
    }
  }
  printf("reached %llu and missed %llu out of %llu considered and 1+%llu total\n", reached, notreached, count, MX);
  return;
}

int main() {
  if (ALL)
    test_all_configs();
  else
    test_input_configs();
  return 0;
}

