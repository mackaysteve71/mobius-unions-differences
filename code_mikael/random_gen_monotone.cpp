#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <map>
#include <deque>
#include<algorithm>

// Works only for N<= 6 because ull...
#define N 6

using namespace std;
typedef unsigned long long ull;


vector<vector<int>> all_perms;
vector<vector<int>> all_renamings;
map<int,ull> cones;

unordered_set<ull> seen;

void print_val(ull val) {
  for (int i = 0; i < (1<<N); i++) {
    printf("%llu", val%2);
    val = val >> 1;
  }
}

void print_vect(vector<int> vect){
  for (int i=0; i< static_cast<int>(vect.size()); ++i)
    cout << vect[i] << " ";
  }

void generate_all_perms(){
  vector<int> perm(N);
  for (int i = 0; i<N; ++i) perm[i]=i;//the initial permutation.
  do {
    all_perms.push_back(perm);
      } while(next_permutation(perm.begin(), perm.end()));//this modifies in place the vector
}


ull rename_set(int set, const vector<int> &perm){
  int renamed_set = 0;
  for (int i=0; i< N; ++i){
    renamed_set += (set & (1 << i)) ? (1 << perm[i]) : 0;
  }
  return renamed_set;
}


void generate_all_renamings(){
  for (vector<int> &perm: all_perms){
    vector<int> renaming(1 << (N));
    for(int set = 0; set < (1 << N); ++set){
      renaming[set] = rename_set(set, perm);
    }
    all_renamings.push_back(renaming);
  }
}

ull rename_conf(ull conf, vector<int> &ren){
  ull ret = 0;
  for (int i=0; i < (1<<N); ++i){
    if (conf & (1<<i)) ret |= ((ull)1 << ren[i]);
  }
  return ret;
}

ull min_representative(ull new_monotone){
  ull ret = new_monotone;
  ull tmp;
  for (vector<int> &ren: all_renamings){
    tmp = rename_conf(new_monotone, ren);
    if (tmp < ret) ret = tmp;
  }
  return ret;
}

int main(){
  //Initialize the cones
  for (int i=0; i< (1<<N); ++i){
    ull cone_i = 0;
    for (int j=i; j< (1<<N); ++j){
      if ((j&i) == i) cone_i |= ((ull) 1 << j);
    }
    cones[i] = cone_i;
  }
//  for (pair<int,ull> el: cones){
//    cout << "Cone " << el.first << " is: "; print_val(el.second); cout << endl;
//  }
  generate_all_perms();
//  for (auto &perm: all_perms){
//    print_vect(perm);
//    cout << endl;
//  }
  generate_all_renamings();
//  for (auto &ren: all_renamings){
//    print_vect(ren);
//    cout << endl;
//  }

  while (true){
    //cout << "Try to generate a new non-constant monotone config...\n";
    ull new_monotone = 0;
    vector<int> min_clauses;
    int all_used=0;
    while (all_used != (1<<N) -1){
      //cout << "Try to select a new clause...\n";
      int next_clause;
      bool subsume = true;
      while (subsume){//find a clause that is not subsumed by a current clause nor is subsumed by a current clause
        next_clause = rand()%((1<<N)-1) +1;
        subsume = false;
        for (int cl: min_clauses){
          if (((cl & next_clause) == next_clause) || ((cl & next_clause) == cl)){
            subsume = true;
            break;
          }
        }
      }
      min_clauses.push_back(next_clause);
      new_monotone |= cones[next_clause];
      all_used |= next_clause;
    }
    //print_val(new_monotone);
    //cout << " whose representative is ";
    new_monotone = min_representative(new_monotone);
    //print_val(new_monotone); cout << endl;
    if (seen.find(new_monotone) == seen.end()){
      seen.insert(new_monotone);
      print_val(new_monotone);
      cout << endl;
    }
  }
  

}
